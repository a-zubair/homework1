<?php

/* 
 * Write a switch case statement to decide on student grades.
 * Note: pass mark is 60. up to 70 is D, up to 80 is C, up to 90 is B 
 * and up to 100 is A.
 */

//solution

$i = 60;//insert a number here
switch($i){
    case 60:
        echo "you are pass!";
        break;
    
    case ($i >= 60 && $i <=70):
        echo "D";
        break;
    
    case ($i >= 71 && $i <= 80):
        echo "C";
        break;
    
    case ($i >= 81 && $i <= 90):
        echo "B";
        break;
    
    case ($i >= 91 && $i <= 100):
        echo "A";
        break;
    
    default :
        echo "worng number, try again!";
}
