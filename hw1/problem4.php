<?php

/* 
 * Mr. Rahim bought some lottery tickets. The lottery draw has been published
 * and it is said that, if the total sum of the digits in the lottery is a 
 * multiple of 7 then they will win 5000 Taka. Write a program to help 
 * Mr. Rahim to find out if he won.
 */

//solution

$lotteryNum = [1330, 5460, 7522];//lottery digits
$sum = array_sum($lotteryNum);

if($sum % 7){
    echo "you win 5000tk!";
}else{
    echo "you lose!";
}